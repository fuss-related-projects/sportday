# -*- coding: utf-8 -*-
# SportDay, an athletic's day organizator
# File: mockup.py
#
# Copyright (c) 2017 Marco Marinello <mmarinello@sezf.it>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# This scripts allows you to populate the db to do some tests
# Usage: python manage.py shell -c 'import mockup'

from sportday import models
from django.contrib.auth.models import User
from datetime import datetime, date
import random, sys

# Some names (for random generation)
nomi = ['Adriano', 'Agatha', 'Alberto', 'Alessandro', 'Alessia', 'Alice', 'Anchise', 'Andrea', 'Angela', 'Anna', 'Anja', 'Aurora', 'Azzurra', 'Barbara', 'Beatrice', 'Bianca', 'Bryan', 'Camilla', 'Carla', 'Caroline', 'Cecilia', 'Chanel', 'Chiara', 'Christian', 'Claudio', 'Costanza', 'Cristina', 'Daniela', 'Debora', 'Denise', 'Dennis', 'Desiderio', 'Diana', 'Diego', 'Donatella', 'Ed', 'Elena', 'Eleonora', 'Elia', 'Elisa', 'Elizabeth', 'Emma', 'Ettore', 'Evelyn', 'Fabio', 'Federico', 'Franca', 'Francesco', 'Frida', 'Fulvio', 'Gino', 'Giordano', 'Giorgia', 'Giulia', 'Giuseppe', 'Grace', 'Harald', 'Iris', 'Jacopo', 'Kevin', 'Lara', 'Laura', 'Leonardo', 'Lidia', 'Lorenzo', 'Luigi', 'Marco', 'Mario', 'Martina', 'Martino', 'Massimo', 'Matteo', 'Maurizio', 'Mauro', 'Michelangelo', 'Michele', 'Mirella', 'Mohammed', 'Nicole', 'Nina', 'Nunziatina', 'Paola', 'Paolo', 'Paride', 'Piergiorgio', 'Piero', 'Piero', 'Pietro', 'Riccardo', 'Rossana', 'Sandra', 'Sara', 'Silvia', 'Simone', 'Sofia', 'Stefano', 'Teresa', 'Valentina', 'Vincenzo', 'Viola', 'Walther']
cognomi = ['Ferri', 'Moretti', 'Greco', 'Piccolo', 'Brambilla', 'Conte', 'Rizzi', 'Guerra', 'Caputo', 'Di Giovanni', 'Sorrentino', 'Pellegrini', 'Perrone', 'Marini', 'Piazza', 'Ruggiero', 'Guerra', 'Monaco', 'Sala', 'Ferrero', 'Agostini', 'Ruggiero', 'Fontana', 'Brambilla', 'Lombardi', 'Palumbo', 'Mariani', 'Morelli', 'Rizzi', 'Moro', 'Vitali', 'Pagano', 'Testa', 'Rinaldi', 'Cavallo', 'Giordano', 'Villani', 'Pinna', 'Bianco', 'Marini', 'Messina', 'Baldi', 'Marino', 'Longo', 'Galli', 'Leone', 'Rossetti', 'Mele', 'Palmieri', 'Mazza', 'Calabrese', 'Perrone', 'Bernardi', 'Esposito', 'Barbieri', 'Sorrentino', 'Pastore', 'Pellegrini', 'Battaglia', 'Marchetti', 'Castelli', 'Antonelli', 'Longo', 'Ferrante', 'Caputo', 'Olivieri', 'Longo', 'Piazza', 'Volpe', 'Guerra', 'Parisi', 'Fumagalli', 'Costantini', 'Martini', 'Bruno', 'Gatti', 'De Simone', 'Esposito', 'Marchetti', 'Valente', 'Ferrero', 'Giuliani', 'Santoro', 'Benedetti', 'Grassi', 'Piazza', 'Grasso', 'Rossi', 'Piazza', 'Albanese', 'Villani', 'Riva', 'Pastore', 'Ferro', 'Vitale', 'Pozzi', 'Parisi', 'Cattaneo', 'Arena', 'Caputo', 'Fontana']


# Create school
if not models.School.objects.all():
    school = models.School.objects.create(name="Liceo P. Cemin", code="LSPR01")
    sys.stdout.write("Created school P. Cemin\n")
else:
    school = models.School.objects.all()[0]


# Create classes
if not models.Class.objects.all():
    for i in range(1, 6):
        for e in ["A", "B", "C", "D", "E"]:
            models.Class.objects.create(
                name=str(i)+e,
                code=e.lower()+str(18-int(i)),
                school=school
            )
    sys.stdout.write("Created all classes")


# Create students
if not models.Student.objects.all():
    sys.stdout.write('Generating students ')
    sys.stdout.flush()
    for cls in models.Class.objects.all():
        for i in range(random.randint(15,27)):
            name = random.choice(nomi)
            models.Student.objects.create(
                last_name=random.choice(cognomi),
                first_name=name,
                date_of_birth=date(
                    year=int(cls.code[1:])+1986,
                    month=random.randint(1,12),
                    day=random.randint(1,28)
                ),
                schoolclass=cls,
                sex=(name[-1] == "a" and "F" or "M")
            )
            sys.stdout.write('.')
            sys.stdout.flush()
    sys.stdout.write(' Done\n')
    sys.stdout.flush()


# Create competitions
if not models.Competition.objects.all():
    sys.stdout.write('Installing competitions ')
    sys.stdout.flush()
    for c in ['100 metri',
              '110 metri ostacoli']:
        models.Competition.objects.create(name=c, result_type="t")
    for c in ['Getto del peso',
              'Salto in lungo']:
        models.Competition.objects.create(name=c, result_type="m")


# Generate partecipations
sys.stdout.write('Generating random partecipations')
sys.stdout.flush()
for x in range(len(models.Student.objects.all())):
    st = random.choice(models.Student.objects.all())
    try:
        models.Partecipation.objects.create(
            student=st,
            competition=random.choice(models.Competition.objects.all())
        )
        sys.stdout.write('.')
        sys.stdout.flush()
    except:
        pass

print("Done")

# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from sportday.models import School, Account


# Some danger message
def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


def landing(request):
    if request.user.is_authenticated:
        return redirect("main-index")
    return render(request, "login/landing.html")


def main(request):
    if not request.user.is_authenticated:
        return redirect("landing")
    return redirect("sportday:index")


def logon(request):
    if request.POST:
        if "username" in request.POST and "password" in request.POST:
            user = authenticate(
                request,
                username=request.POST["username"],
                password=request.POST["password"]
            )
            if user:
                login(request, user)
                messages.success(
                    request,
                    _("Benvenuto, %s!") % (user.get_full_name() or user.username)
                )
                _n = request.POST.get("next", "main-index")
                return redirect(_n)
            else:
                messages.danger(
                    request,
                    _("Username o password errati o utente inesistente o inattivo.")
                )
                if request.POST.get("nextfail", False):
                    return redirect(request.POST["nextfail"])
    return render(
        request,
        "login/index.html",
        {
            "schools": School.objects.all(),
            "next": request.GET.get("next", "main-index")
        }
    )


@login_required
def logoff(request):
    logout(request)
    messages.success(
        request,
        _("Sessione terminata. A presto!")
    )
    return redirect("main-index")


@login_required
def password_change(request):
    if request.POST:
        pws = [request.POST.get(x) for x in ("pw", "pwc")]
        if pws[0] != pws[1]:
            messages.warning(request, _("Le due password non coincidono!"))
            return render(request, "sportday/pwchange.html")
        if len(pws[0]) < 8:
            messages.warning(request, _("La password è troppo corta!"))
            return render(request, "sportday/pwchange.html")
        u = request.user
        u.set_password(pws[0])
        u.save()
        a = Account.objects.get(user=u)
        a.first_login = False
        a.save()
        update_session_auth_hash(request, u)
        messages.success(request, _("Password aggiornata con successo"))
        return redirect("main-index")
    return render(request, "sportday/pwchange.html")

from django.apps import AppConfig


class SportdayConfig(AppConfig):
    name = 'sportday'

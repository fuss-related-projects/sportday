# -*- coding: utf-8 -*-
# Importazione dell'elenco dei corsi
# Copyright (c) 2017-2018 Marco Marinello <marco.marinello@fuss.bz.it>
# Copyright (c) 2017-2018 Piergiorgio Cemin <pcemin@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
import csv
from django.contrib.auth.models import User
from sportday.models import Student
from sportday.models import Class

global pgbar

try:
    from progressbar import ProgressBar
    pgbar = True
except:
    pgbar = False


class Command(BaseCommand):
    help = "Importa gli alunni da una lista csv"

    def add_arguments(self, parser):
        parser.add_argument('nome_file', type=str)
        parser.add_argument(
            '--delimiter',
            dest='delimiter',
            default=";",
            type=str
        )
        parser.add_argument(
            '--debug',
            dest='debug',
            action='store_true',
        )

    def handle(self, *args, **kwargs):
        DEBUG = False
        if 'debug' in kwargs:
            DEBUG = kwargs['debug']

        global pgbar
        pgbar = pgbar and not DEBUG or False

        nome_file = kwargs['nome_file']
        header = open(nome_file)

        if pgbar:
            pgbar = ProgressBar(max_value=len(header.readlines()))

        reader = csv.reader(header, delimiter=kwargs['delimiter'])

        now = 0

        for row in reader:
            last_name = row[0]
            first_name = row[1]
            date_of_birth = row[2]
            sex = row[3]
            cl = row[4]
            schoolclass = Class.objects.get(code=cl)
            
            		

            
            studente = Student.objects.create (
                last_name = last_name,
            	first_name = first_name,
            	date_of_birth = date_of_birth,
            	sex = sex,
            	schoolclass = schoolclass,
                
                )
            now += 1
            if pgbar: pgbar.update(now)
            elif DEBUG:
                print("Importato", "#%s:" % last_name, first_name)

        header.close()

        print("\nFatto.")

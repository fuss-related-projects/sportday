# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf.urls import url, include
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^class/(?P<id>[0-9]+)$', views.class_show, name='class'),
    url(r'^class/(?P<id>[0-9]+)/ajax$', views.class_show_ajax, name='class-ajax'),
    url(r'^class/(?P<id>[0-9]+)/get_student$',
        views.student_get,
        name='get-student'
    ),
    url(r'^class/(?P<id>[0-9]+)/save_student$',
        views.student_post,
        name='save-student'
    ),
    url(r'^class/(?P<id>[0-9]+)/report$',
        views.class_report,
        name='class-report'
    ),
    url(r'^class/(?P<id>[0-9]+)/add_spokeperson', views.add_spokeperson, name='add_spokeperson'),
    url(r'^class/choice$', views.class_choice, name='class-choice'),
    url(r'^competition/choice$',
        views.competition_choice,
        name='competition-choice'
    ),
    url(r'^competition/(?P<id>[0-9]+)$',
        views.competition,
        name='competition'
    ),
    url(r'^competition/(?P<id>[0-9]+)/report$',
        views.competition_report,
        name='competition-report'
    ),
    url(r'^competition/(?P<id>[0-9]+)/ajax$',
        views.competition_ajax,
        name='competition-ajax'
    ),
    url(r'^competition/(?P<id>[0-9]+)/act$',
        views.part_act,
        name='partecipation-action'
    ),
    url(r'^ranking/choice$', views.ranking_choice, name='ranking-choice'),
    url(r'^ranking/print.pdf$', views.ranking, name='competition-ranking'),
]

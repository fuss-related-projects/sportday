# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _


class School(models.Model):
    name = models.CharField(
        max_length=100,
        unique=True,
        verbose_name=_("name")
    )

    code = models.CharField(
        max_length=20,
        verbose_name=_("code"),
        help_text=_("the school code that will be before the user's name")
    )


    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("school")
        verbose_name_plural = _("schools")
        ordering = ("name",)


class Competition(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name=_("name")
    )

    available_in_schools = models.ManyToManyField(
        School,
        verbose_name=_("Available in this schools")
    )

    result_type = models.CharField(
        choices=[("t", _("Time")), ("m", _("Meters"))],
        max_length=1,
        verbose_name=_("result type")
    )


    def __str__(self):
        return self.name


    class Meta:
        verbose_name = _("competition")
        verbose_name_plural = _("competitions")
        ordering = ("name",)


class Class(models.Model):
    code = models.CharField(
        max_length=10,
        unique=True,
        verbose_name=_("code")
    )

    name = models.CharField(
        max_length=10,
        verbose_name=_("name")
    )

    school = models.ForeignKey(
        School,
        on_delete=models.DO_NOTHING,
        verbose_name=_("school")
    )


    def __str__(self):
        return "%s %s" % (self.name, self.school)


    class Meta:
        verbose_name = _("class")
        verbose_name_plural = _("classes")
        ordering = ("school", "name", "code",)


class Student(models.Model):
    last_name = models.CharField(
        max_length=200,
        verbose_name=_("last name")
    )

    first_name = models.CharField(
        max_length=200,
        verbose_name=_("first name")
    )

    date_of_birth = models.DateField(
        verbose_name=_("date of birth")
    )

    schoolclass = models.ForeignKey(
        Class,
        on_delete=models.DO_NOTHING,
        verbose_name=_("class")
    )

    choices = [
        ("M", _("male")),
        ("F", _("female"))
    ]

    sex = models.CharField(
        max_length=2,
        choices=choices,
        verbose_name=_("sex")
    )


    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def __str__(self):
        return self.get_full_name()


    class Meta:
        verbose_name = _("student")
        verbose_name_plural = _("students")
        ordering = ("schoolclass", "last_name", "first_name",)


class Account(models.Model):
    user = models.ForeignKey(
        "auth.User",
        on_delete=models.DO_NOTHING,
        verbose_name=_("user")
    )

    classes = models.ManyToManyField(
        Class,
        blank=True,
        verbose_name=_("classes")
    )

    first_login = models.BooleanField(
        default=True,
        verbose_name=_("User logs in for the first time")
    )

    is_teacher = models.BooleanField(
        default=False,
        verbose_name=_("User is a teacher")
    )

    inputs_results = models.BooleanField(
        default=False,
        verbose_name=_("User inserts competition results")
    )


    def __str__(self):
        return _("%s's user") % self.user.username


    def determine_school(self):
        schools = []
        for c in self.classes.all():
            if c.school not in schools:
                schools.append(c.school)
        return schools


    class Meta:
        verbose_name = _("account")
        verbose_name_plural = _("accounts")


class Partecipation(models.Model):
    student = models.ForeignKey(
        Student,
        on_delete=models.DO_NOTHING,
        verbose_name=_("student")
    )

    competition = models.ForeignKey(
        Competition,
        on_delete=models.DO_NOTHING,
        verbose_name=_("competition")
    )

    time_result = models.DurationField(
        verbose_name=_("result time"),
        blank=True,
        null=True
    )

    meters_result = models.FloatField(
        verbose_name=_("meters result"),
        blank=True,
        null=True
    )


    def __str__(self):
        return "%s %s %s %s" % (
            _("Partecipation of"),
            self.student,
            _("to"),
            self.competition
        )


    class Meta:
        verbose_name = _("partecipation")
        verbose_name_plural = _("partecipations")
        ordering = ("student",)
        unique_together = ("student", "competition")

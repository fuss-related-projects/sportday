# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin
from .models import *
from django.utils.translation import ugettext_lazy as _


class SchoolAdmin(admin.ModelAdmin):
    model = School
    search_fields = ["id", "name", "code"]
    def get_list_display(self, request):
        return ("code", "name")

admin.site.register(School, SchoolAdmin)


def for_all_schools(modeladmin, request, queryset):
    #queryset.update(available_in_schools=)
    schools = list(School.objects.all())
    for i in queryset:
        i.available_in_schools.clear()
        i.available_in_schools.add(*schools)
for_all_schools.short_description = _("Available in all schools")


class CompetitionAdmin(admin.ModelAdmin):
    model = Competition
    search_fields = ["id", "name", "result_type"]
    def get_list_display(self, request):
        return ("name", "result_type")
    actions = [for_all_schools]

admin.site.register(Competition, CompetitionAdmin)


class ClassAdmin(admin.ModelAdmin):
    model = Class
    search_fields = ["id", "name", "code", "school__name"]
    def get_list_display(self, request):
        return ("name", "code", "school")

admin.site.register(Class, ClassAdmin)


class StudentAdmin(admin.ModelAdmin):
    model = Student
    search_fields = ["id", "last_name", "first_name", "date_of_birth",
                     "schoolclass__name", "schoolclass__school__name"]
    list_filter = ["sex"]
    def get_list_display(self, request):
        return ("last_name", "first_name", "schoolclass")

admin.site.register(Student, StudentAdmin)


class AccountAdmin(admin.ModelAdmin):
    model = Account
    search_fields = ["id", "user", "first_login", "is_teacher", "inputs_results"]
    def get_list_display(self, request):
        return ("user", "first_login", "is_teacher", "inputs_results")

admin.site.register(Account, AccountAdmin)


class PartecipationAdmin(admin.ModelAdmin):
    model = Partecipation
    search_fields = ["id", "student__last_name", "student__first_name",
                     "competition__name", "time_result", "meters_result"]
    def get_list_display(self, request):
        return ("student", "competition")

admin.site.register(Partecipation, PartecipationAdmin)

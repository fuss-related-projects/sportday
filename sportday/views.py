# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from datetime import timedelta
from .models import Account, Class, Student, Competition, Partecipation
from .decorators import has_sportday_account, admins_this_class, \
     teacher_or_result
import json
from . import report


# Some danger message
def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


@login_required
@has_sportday_account
def index(request, account):
    if account.is_teacher or account.inputs_results:
        return render(request, "sportday/landing.html", {"account": account})
    if len(account.classes.all()) > 1:
        return redirect("sportday:class-choice")
    return redirect("sportday:class", id=account.classes.all()[0].id)


@login_required
@has_sportday_account
def class_choice(request, account):
    return render(
        request,
        "sportday/class-choice.html",
        {"account": account}
    )


@login_required
@has_sportday_account
@admins_this_class
def class_show(request, id=None, account=None):
    cl = get_object_or_404(Class, id=id)
    students = Student.objects.filter(schoolclass=cl)
    competitions = Competition.objects.filter(
        available_in_schools__in=[cl.school]
    )
    return render(
        request,
        "sportday/class.html",
        {"class": cl, "students": students, "competitions": competitions, "show_private": account.is_teacher}
    )


def select_icon(student):
    if len(Partecipation.objects.filter(student=student)) > 0:
        return "<i class='fa fa-check fa-2x green'></i>"
    return "<i class='fa fa-times fa-2x red'></i>"


def call_show_student_btn(student):
    return ('<i class="fa fa-pencil fa-2x" '+
    'onclick="loadStudent(\'%s\');" title="'+
    '%s %s" data-toggle="modal" data-target="#studentbox"></i>') % (
        student.id, _('Visualizza la scheda di'), student.get_full_name())


@login_required
@has_sportday_account
@admins_this_class
def class_show_ajax(request, id, account):
    # Fetch vars from request
    start = int(request.GET.get("iDisplayStart", "0"))
    length = int(request.GET.get("iDisplayLength", "30"))
    query = request.GET.get("sSearch", False)

    # Compute data
    db = Student.objects.filter(schoolclass_id=id)
    all_length = len(db)
    if query:
        db = db.filter(Q(last_name__icontains=query)|
                       Q(first_name__icontains=query))
    full_length = len(db)

    students = [[
        select_icon(student),
        ("%s %s" % (student.last_name, student.first_name)).strip(),
        account.is_teacher and student.date_of_birth.strftime("%d/%m/%Y") or "",
        call_show_student_btn(student)
        ] for student in db[start:start+length]]

    return HttpResponse(
        json.dumps(
            {"aaData": students,
             "iTotalDisplayRecords": len(db),
             "iTotalRecords": all_length,
             "sEcho": None
             }
            )
        )


@login_required
@has_sportday_account
@admins_this_class
def add_spokeperson(request, id, account):
    if not account.is_teacher:
        messages.danger(request, _("Solo i docenti possono aggiungere nuovi rappresentanti!"))
        return redirect("sportday:class", id=id)
    data = [request.POST.get(x) for x in ("last_name", "first_name", "email")]
    if None in data:
        messages.danger(request, _("Prego compilare tutti i campi!"))
        return redirect("sportday:class", id=id)
    try:
        this_class = Class.objects.get(id=id)
        username = "%s%s" %( data[0], data[1][:3] )
        username = "%s.%s" % (this_class.school.code, username.lower())
        u = User.objects.create(
            username=username,
            last_name=data[0].title(),
            first_name=data[1].title(),
            email=data[2]
        )
        password = User.objects.make_random_password()
        u.set_password(password)
        u.save()
        cl = Account.objects.create(
            user=u
        )
        cl.classes.add(this_class)
        cl.save()
        messages.success(request, _("Rappresentante aggiunto con successo."))
        return render(request, "sportday/newspokeperson.html", {"newuser": cl, "pw": password})
    except Exception as E:
        messages.danger(request, "%s" % E)
    return redirect("sportday:class", id=id)


def render_partecipation_list(student):
    return [a.competition.id for a in Partecipation.objects.filter(
        student=student)]


@login_required
@has_sportday_account
@admins_this_class
def student_get(request, id, account):
    student = request.POST.get("student", False)
    if not student:
        return HttpResponse(json.dumps({
            "status": "error", "error": "Missing student id"
        }))
    try:
        st = Student.objects.get(id=student, schoolclass__id=id)
        return HttpResponse(
            json.dumps(
                {"last_name": st.last_name,
                 "first_name": st.first_name,
                 "full_name": st.get_full_name(),
                 "date_of_birth": account.is_teacher and st.date_of_birth.strftime("%d/%m/%Y") or str(_("nascosta")),
                 "competitions": render_partecipation_list(st),
                 "status": "success"
                }
            )
        )
    except Exception as e:
        return HttpResponse(json.dumps({
            "status": "error", "error": str(e)
        }))


@login_required
@has_sportday_account
@admins_this_class
def student_post(request, id, account):
    student = request.POST.get("student", False)
    if not student:
        return HttpResponse(json.dumps({
            "status": "error", "error": "Missing student id"
        }))
    try:
        student = Student.objects.get(id=request.POST["student"], schoolclass__id=id)
        # Cleanup all partecipations
        Partecipation.objects.filter(student=student).delete()
        # Recreate requested partecipations
        for i in json.loads(request.POST["competitions"]):
            Partecipation.objects.create(student=student, competition_id=int(i))
        return HttpResponse(json.dumps({
            "status": "success"
        }))
    except Exception as e:
        return HttpResponse(json.dumps({
            "status": "error", "error": str(e)
        }))


@login_required
@has_sportday_account
@admins_this_class
def class_report(request, id, account):
    cl = get_object_or_404(Class, id=id)
    return report.class_report(request, cl, account.is_teacher)


@login_required
@has_sportday_account
@teacher_or_result
def competition_choice(request, account):
    return render(
        request,
        "sportday/competitions.html",
        {"competitions": Competition.objects.all()}
    )


@login_required
@has_sportday_account
@teacher_or_result
def competition(request, account, id):
    c = get_object_or_404(Competition, id=id)
    cats_pre = settings.CATEGORIES.keys()
    colors = ["warning", "info", "dark", "secondary"]
    cats = []
    i = 0
    for a in cats_pre:
        try:
            cats.append([a, colors[i]])
        except:
            cats.append([a, colors[0]])
        i += 1
    return render(
        request,
        "sportday/competition.html",
        {"competition": c, "cats": cats}
    )


@login_required
@has_sportday_account
@teacher_or_result
def competition_report(request, account, id):
    if not request.POST:
        return HttpResponse("404: Not found", status=404)
    c = get_object_or_404(Competition, id=id)
    db = Partecipation.objects.filter(competition=c, student__schoolclass__school__in=account.determine_school())
    cat = ""
    if request.POST["cat"] != "all":
        cat = request.POST["cat"]
        years = settings.CATEGORIES[request.POST["cat"]]
        db = db.filter(student__date_of_birth__year__in=years)
    if request.POST["sex"] != "all":
        cat += request.POST["sex"]
        db = db.filter(student__sex=request.POST["sex"].upper())
    return report.competition_report(request, c, db, cat.upper())


def aprop_links(account, partecipation):
    name = partecipation.student.get_full_name().replace("'", "&#39;")
    link = ""
    if partecipation.time_result or partecipation.meters_result:
        link = "<a class='btn btn-lg "
        link += "btn-success fa fa-check"
        link += " fa-nx btn-table' title='%s' " % (
            _("Modifica partecipazione di %(name)s (risultato: %(r)s)") % {
                    "name": name,
                    "r": (partecipation.time_result or partecipation.meters_result)
                }
        )
        link += "onclick='loadInsDataModal(\"%s\", \"%s\", \"%s\");'></a>" % (
            partecipation.id, name,
            (partecipation.time_result or partecipation.meters_result)
        )
    else:
        link = "<a class='btn btn-lg "
        link += "btn-primary fa fa-pencil"
        link += " fa-nx btn-table' title='%s' " % (
            _("Modifica partecipazione di %s") % name
        )
        link += "onclick='loadInsDataModal(\"%s\", \"%s\");'></a>" % (
            partecipation.id, name
        )
    if account.is_teacher:
        link += "<a class='btn btn-lg btn-danger fa fa-close fa-nx btn-table' "
        link += "onclick='delPart(\"%s\", \"%s\");'" % (partecipation.id, name)
        link += " title='%s'></a>" % (
            _('Cancella la partecipazione di %s') % name
        )
    return link


@login_required
@has_sportday_account
@teacher_or_result
def competition_ajax(request, account, id):
    # Fetch vars from request
    start = int(request.GET.get("iDisplayStart", "0"))
    length = int(request.GET.get("iDisplayLength", "30"))
    query = request.GET.get("sSearch", False)

    # Compute data
    db = Partecipation.objects.filter(competition_id=id, student__schoolclass__school__in=account.determine_school())
    all_length = len(db)
    if query:
        db = db.filter(Q(student__last_name__icontains=query)|
                       Q(student__first_name__icontains=query)|
                       Q(student__schoolclass__name__icontains=query))
    full_length = len(db)

    students = [[
        ("%s %s" % (a.student.last_name, a.student.first_name)).strip(),
        a.student.schoolclass.name,
        aprop_links(account, a)
        ] for a in db[start:start+length]]

    return HttpResponse(
        json.dumps(
            {"aaData": students,
             "iTotalDisplayRecords": len(db),
             "iTotalRecords": all_length,
             "sEcho": None
             }
            )
        )


@login_required
@has_sportday_account
@teacher_or_result
def part_act(request, account, id):
    try:
        act = request.POST["action"]
        if act == "drop":
            Partecipation.objects.get(
                competition_id=id,
                id=request.POST["partid"],
                student__schoolclass__school__in=account.determine_school()
            ).delete()
            return HttpResponse(json.dumps({
                "status": "success"
            }))
        elif act == "insert":
            res = float(request.POST["result"])
            part = Partecipation.objects.get(
                competition_id=id,
                id=request.POST["partid"],
                student__schoolclass__school__in=account.determine_school()
            )
            if part.competition.result_type == "t":
                part.time_result = timedelta(seconds=res)
            else:
                part.meters_result = res
            part.save()
            return HttpResponse(json.dumps({
                "status": "success"
            }))
    except Exception as e:
        return HttpResponse(json.dumps({
            "status": "error", "error": str(e)
        }))
    return HttpResponse(json.dumps({
        "status": "error", "error": "Nothing to do."
    }))


@login_required
@has_sportday_account
@teacher_or_result
def ranking_choice(request, account):
    cats_pre = settings.CATEGORIES.keys()
    colors = ["warning", "info", "dark", "secondary"]
    cats = []
    i = 0
    for a in cats_pre:
        try:
            cats.append([a, colors[i]])
        except:
            cats.append([a, colors[0]])
        i += 1
    return render(
        request,
        "sportday/ranking.html",
        {"competitions": Competition.objects.all(), "cats": cats}
    )


@login_required
@has_sportday_account
@teacher_or_result
def ranking(request, account):
    if not request.POST:
        return HttpResponse("404: Not found", status=404)
    comp = get_object_or_404(Competition, id=request.POST["comp-id"])
    db = Partecipation.objects.filter(competition=comp, student__schoolclass__school__in=account.determine_school())
    cat = ""
    if request.POST["cat"] != "all":
        cat = request.POST["cat"]
        years = settings.CATEGORIES[request.POST["cat"]]
        db = db.filter(student__date_of_birth__year__in=years)
    if request.POST["sex"] != "all":
        cat += request.POST["sex"]
        db = db.filter(student__sex=request.POST["sex"].upper())
    return report.competition_ranking(request, comp, db, cat.upper())

# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import redirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from sportday.models import Account, Class


# Some danger message
def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


def has_sportday_account(view, *args, **kwargs):
    def do_check(request, *args, **kwargs):
        try:
            a = Account.objects.get(user=request.user)
        except:
            messages.danger(
                request,
                _("Il suo account non è abilitato all'accesso a questa risorsa. La sessione verrà automaticamente terminata.")
            )
            return redirect("logout")
        if a.first_login:
            return redirect("password_change")
        return view(request, *args, account=a, **kwargs)
    return do_check


def admins_this_class(view, *args, **kwargs):
    def do_check(request, id, *args, **kwargs):
        try:
            a = Account.objects.get(user=request.user)
            c = Class.objects.get(id=id)
            if c not in a.classes.all():
                raise
        except:
            messages.danger(
                request,
                _("Il suo account non è abilitato all'accesso a questa risorsa.")
            )
            return redirect("sportday:index")
        return view(request, id=id, *args, **kwargs)
    return do_check


def teacher_or_result(view, *args, **kwargs):
    def do_check(request, *args, **kwargs):
        try:
            a = Account.objects.get(user=request.user)
            if not (a.is_teacher or a.inputs_results):
                raise
        except:
            messages.danger(
                request,
                _("Il suo account non è abilitato all'accesso a questa risorsa.")
            )
            return redirect("sportday:index")
        return view(request, *args, **kwargs)
    return do_check

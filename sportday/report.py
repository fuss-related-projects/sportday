# Report generator
# -*- coding: utf-8 -*-
# Copyright (c) 2017 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import HttpResponse
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.enums import TA_CENTER, TA_RIGHT
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, \
     ListFlowable, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, \
     _baseFontNameB, _baseFontName
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.utils import ImageReader
from reportlab.pdfbase.ttfonts import TTFont
from django.conf import settings
from os.path import join
from .models import Partecipation, Class, Account


pdfmetrics.registerFont(
    TTFont(
        "MrDeHaviland",
        join(settings.BASE_DIR, "pdfutils", "MrDeHaviland-Regular.ttf")
    )
)


# Get styles
styles = getSampleStyleSheet()

styles.add(
    ParagraphStyle(
        name='subtitle',
        fontName=_baseFontName,
        fontSize=16,
        leading=20,
        alignment=TA_CENTER
    )
)

styles.add(
    ParagraphStyle(
        name='pre-sign',
        fontName=_baseFontNameB,
        fontSize=14,
        leading=18,
        alignment=TA_RIGHT
    )
)

styles.add(
    ParagraphStyle(
        name='sign',
        fontName=_baseFontName,
        fontSize=12,
        leading=16,
        alignment=TA_RIGHT
    )
)

styles.add(
    ParagraphStyle(
        name='signature',
        parent=styles["sign"],
        fontName="MrDeHaviland",
        fontSize=30,
        leading=44,
        alignment=TA_RIGHT
    )
)

styles.add(
    ParagraphStyle(
        name='rights',
        fontName=_baseFontName,
        fontSize=8,
        leading=10,
        alignment=TA_RIGHT
    )
)

styles.add(
    ParagraphStyle(
        name='h5c',
        parent=styles['h5'],
        fontName=_baseFontNameB,
        alignment=TA_CENTER
    )
)

styles.add(
    ParagraphStyle(
        name='NormalC',
        parent=styles['Normal'],
        alignment=TA_CENTER
    )
)

def Page(canvas, doc):
    bg = ImageReader(join(
        settings.BASE_DIR,
        "pdfutils", "filigrana.png"
    ))
    width, height = bg.getSize()
    canvas.drawImage(bg, 0, 0, A4[0], A4[1])


def recursive_search(table, student):
    i = 1
    while True:
        if table[-i][0].text != "":
            if table[-i][0].text == student:
                return True
            return False
        i += 1


def class_report(request, schoolclass, dates_of_birth=False):
    response = HttpResponse(content_type='application/pdf')
    fn = "partecipazioni-%s.pdf" % ''.join(i for i in schoolclass.code if i.isalnum())
    response['Content-Disposition'] = 'filename="%s"' % fn

    # Create canvas
    doc = SimpleDocTemplate(response, pagesize=A4)

    # Create document list (to generate)
    document = []

    # Add class
    document.append(Paragraph(
        schoolclass.__str__(), styles["title"]
    ))
    document.append(Paragraph(
        "Riassunto partecipazioni gare", styles["subtitle"]
    ))

    # Table
    head = [
        "COGNOME E NOME",
        "SESSO",
        dates_of_birth and "DATA DI NASCITA" or "",
        "GARA"
    ]
    table = [[Paragraph(i, styles["h5c"]) for i in head]]

    for i in Partecipation.objects.filter(
        student__schoolclass=schoolclass).order_by("student"):
        stname = "%s %s" %(i.student.last_name, i.student.first_name)
        if recursive_search(table, stname):
            table.append([Paragraph(i, styles["NormalC"]) for i in [
                "",
                "",
                "",
                str(i.competition)
            ]])
        else:
            table.append([Paragraph(i, styles["NormalC"]) for i in [
                stname,
                i.student.sex,
                dates_of_birth and i.student.date_of_birth.strftime("%d/%m/%Y") or "",
                str(i.competition)
            ]])

    _t = Table(table, colWidths=[None, 45, 100, None])
    _t.setStyle(TableStyle([
        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
        ('BOX', (0,0), (-1,-1), 0.25, colors.black)
    ]))

    document.append(Spacer(1, 20))
    document.append(_t)

    # Sign
    document.append(Spacer(1, 20))
    document.append(Paragraph(
        "I rappresentanti di classe", styles["pre-sign"]
    ))
    for i in Account.objects.filter(
        classes=schoolclass, is_teacher=False):
        document.append(Paragraph(i.user.get_full_name(), styles["signature"]))

    # Build document
    doc.build(document, onFirstPage=Page, onLaterPages=Page)

    return response


def competition_report(request, comp, db, category=""):
    response = HttpResponse(content_type='application/pdf')
    fn = "partecipazioni-%s.pdf" % ''.join(i for i in comp.name if i.isalnum())
    response['Content-Disposition'] = 'filename="%s"' % fn

    # Create canvas
    doc = SimpleDocTemplate(response, pagesize=A4)

    # Create document list (to generate)
    document = []

    # Add competition
    document.append(Paragraph(
        comp.name + (category and " %s" % category), styles["title"]
    ))
    document.append(Paragraph(
        "Riassunto partecipanti", styles["subtitle"]
    ))

    # Table
    head = [
        "COGNOME E NOME",
        "SESSO",
        "DATA DI NASCITA",
        "CLASSE",
        "RISULTATO"
    ]
    table = [[Paragraph(i, styles["h5c"]) for i in head]]

    for i in db:
        table.append([Paragraph(i, styles["NormalC"]) for i in [
            "%s %s" %(i.student.last_name, i.student.first_name),
            i.student.sex,
            i.student.date_of_birth.strftime("%d/%m/%Y"),
            i.student.schoolclass.name,
            ""
        ]])

    _t = Table(table, colWidths=[None, 45, 100, 60, None])
    _t.setStyle(TableStyle([
        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
        ('BOX', (0,0), (-1,-1), 0.25, colors.black)
    ]))

    document.append(Spacer(1, 20))
    document.append(_t)

    # Sign
    document.append(Spacer(1, 20))
    document.append(Paragraph(
        "Il giudice di gara", styles["pre-sign"]
    ))
    document.append(Spacer(1, 10))
    document.append(Paragraph(
        "_"*25, styles["sign"]
    ))

    # Build document
    doc.build(document, onFirstPage=Page, onLaterPages=Page)

    return response


def competition_ranking(request, comp, db, category=""):
    response = HttpResponse(content_type='application/pdf')
    fn = "classifica-%s.pdf" % ''.join(i for i in comp.name if i.isalnum())
    response['Content-Disposition'] = 'filename="%s"' % fn

    # Create canvas
    doc = SimpleDocTemplate(response, pagesize=A4)

    # Create document list (to generate)
    document = []

    # Add competition
    document.append(Paragraph(
        comp.name + (category and " %s" % category), styles["title"]
    ))
    document.append(Paragraph(
        "Classifica", styles["subtitle"]
    ))

    unit = ""
    if comp.result_type == "m":
        db = db.exclude(meters_result=None).order_by("-meters_result")
        unit = "m"
    else:
        db = db.exclude(time_result=None).order_by("time_result")

    # Table
    head = [
        "#",
        "COGNOME E NOME",
        "SESSO",
        "CLASSE",
        "DATA DI NASCITA",
        "RISULTATO"
    ]
    table = [[Paragraph(i, styles["h5c"]) for i in head]]

    n = 1
    for i in db:
        r = ("%s %s" % ((i.time_result or i.meters_result), unit)).strip()
        _n = n
        if table[-1][-1].text == r:
            _n = table[-1][0].text
        table.append([Paragraph(i, styles["NormalC"]) for i in [
            str(_n),
            "%s %s" %(i.student.last_name, i.student.first_name),
            i.student.sex,
            i.student.schoolclass.name,
            i.student.date_of_birth.strftime("%d/%m/%Y"),
            r
        ]])
        n += 1

    _t = Table(table, colWidths=[30, None, 45, 50, None, None])
    _t.setStyle(TableStyle([
        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
        ('BOX', (0,0), (-1,-1), 0.25, colors.black)
    ]))

    document.append(Spacer(1, 20))
    document.append(_t)

    # Sign
    document.append(Spacer(1, 20))
    document.append(Paragraph(
        "Il giudice di gara", styles["pre-sign"]
    ))
    document.append(Spacer(1, 10))
    document.append(Paragraph(
        "_"*25, styles["sign"]
    ))

    # Build document
    doc.build(document, onFirstPage=Page, onLaterPages=Page)

    return response
